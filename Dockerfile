# ----------------------------------
# Pterodactyl Core Dockerfile
# Environment: NodeJS (glibc support)
# Minimum Panel Version: 0.6.0
# ----------------------------------
FROM        node:alpine

LABEL       author="Alexander Da Costa" maintainer="wingflyer.business@gmail.com"

RUN apk update \
 && apk add --no-cache --update libc6-compat ffmpeg curl ca-certificates openssl git tar bash sqlite fontconfig tzdata iproute2 \
 && adduser -D -h /home/container container \
 
USER container
ENV  USER=container HOME=/home/container

USER        container
ENV         USER=container HOME=/home/container

WORKDIR     /home/container

COPY        ./entrypoint.sh /entrypoint.sh

CMD         ["/bin/bash", "/entrypoint.sh"]
